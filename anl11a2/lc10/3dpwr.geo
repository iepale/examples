/* 
                        BENCHMARK PROBLEM
           
  Identification: 11-A2          Source Situation ID.11
  Date Submitted: June 1976      By: R. R. Lee (CE)
                                     D. A. Menely (Ontario Hydro)
                                     B. Micheelsen (Riso-Denmark)
                                     D. R. Vondy (ORNL)
                                     M. R. Wagner (KWU)
                                     W. Werner (GRS-Munich)
 
  Date Accepted:  June 1977      By: H. L. Dodds, Jr. (U. of Tenn.)
                                     M. V. Gregory (SRL)
 
  Descriptive Title: Three-dimensional LWR Problem,
                     also 3D IAEA Benchmark Problem
 
*/

// Macro to set a equal spaced point on a line in the xy plane at z=190.
//The input variables are:
//dirx: x direction and x distance between neighbour points.
//diry: y direction and y distance between neoghbour points.
//x0: initial point in x.
//y0: initial point in y.
//npoints: the number of points.
//The output variables are:
//listpoints[]: all the points number in the same order as the were created.
Macro PointLine
For i In {0:npoints-1}
  listpoints += {newp};
  Point(listpoints[i]) = { x0 + i * dirx , y0 + i * diry , 190.0, lc};
//  Printf("%g",i);
EndFor
Return
// Macro to draw the triangular assemblies boundary and its surfaces.
// i = Column
// The output variables are:
// boun_i_i: boundary line of the assembly i i
// surf_i_i: surface of the the assembly i i
Macro AsBoundTri
boun_1_1 = {};  //Boundary of the assembly 1,1 (1)
l = newl;
Line(l) = {1, row_1[0]};
boun_1_1 += {l};
l = newl;
Line(l) = { row_1[0], row_2[0]};
boun_1_1 += {l};
l = newl;
Line(l) = { row_2[0], 1};
boun_1_1 += {l};
l = newl;
Line Loop(l) = {boun_1_1[]};
s = news;
surf_1_1 = s;
Plane Surface(s) = {l};
For i In {2:7}
  boun~{i}~{i} = {};
  l = newl;
  Line(l) = {row~{i}[0] , row~{i}[1]};
  boun~{i}~{i} += {l};
  l = newl;
  Line(l) = {row~{i}[1] , row~{i+1}[0]};
  boun~{i}~{i} += {l};
  l = newl;
  Line(l) = {row~{i+1}[0] , row~{i}[0]};
  boun~{i}~{i} += {l};
  l = newl;
  Line Loop(l) = {boun~{i}~{i}[]};
  s = news;
  surf~{i}~{i} = s;
  Plane Surface(s) = {        l        };
EndFor
Return
lc =  10.0;  
heigth2 = 190.0;   //half height
startaz = 20.0; //Start of active zone.
hcr = 280.0 ;//Cr height
cri = 360-280 ; //Cr insertion.
uplay = 20.0 ; // Height of upper layer.
//Origin
Point(1) = { 0, 0, 190, lc};
//First horizontal line.
dirx = 20.0;
diry = 0;
x0 = 10;
y0 = 0;
npoints = 9;
listpoints = {};
Call PointLine;
row_1 = {listpoints[]};
//2 horizontal line.
x0 = 10;
y0 = 10;
listpoints = {};
Call PointLine;
row_2 = {listpoints[]};
//3 horizontal line.
x0 = 30;
y0 = 30;
npoints = 8;
listpoints = {};
Call PointLine;
row_3 = {listpoints[]};
//4 horizontal line.
x0 = 50;
y0 = 50;
npoints = 7;
listpoints = {};
Call PointLine;
row_4 = {listpoints[]};
//5 horizontal line.
x0 = 70;
y0 = 70;
npoints = 6;
listpoints = {};
Call PointLine;
row_5 = {listpoints[]};
//6 horizontal line.
x0 = 90;
y0 = 90;
npoints = 4;
listpoints = {};
Call PointLine;
row_6 = {listpoints[]};
//7 horizontal line.
x0 = 110;
y0 = 110;
npoints = 3;
listpoints = {};
Call PointLine;
row_7 = {listpoints[]};
//8 horizontal line, only 1 point.
x0 = 130;
y0 = 130;
npoints = 1;
listpoints = {};
Call PointLine;
row_8 = {listpoints[]};
// External line boundary.
linext = {};   //List with the external lines.
l = newl;
Line(l) = {row_1[#row_1[]-1] , row_2[#row_2[]-1]} ;
linext += {l};
l = newl;
Line(l) = {row_2[#row_2[]-1] , row_3[#row_3[]-1]} ;
linext += {l};
l = newl;
Line(l) = {row_3[#row_3[]-1] , row_4[#row_4[]-1]} ;
linext += {l};
l = newl;
Line(l) = {row_4[#row_4[]-1] , row_5[#row_5[]-1]} ;
linext += {l};
l = newl;
Line(l) = {row_5[#row_5[]-1] , row_5[#row_5[]-2]} ;
linext += {l};
l = newl;
Line(l) = {row_5[#row_5[]-2] , row_6[#row_6[]-1]} ;
linext += {l};
l = newl;
Line(l) = {row_6[#row_6[]-1] , row_7[#row_7[]-1]} ;
linext += {l};
l = newl;
Line(l) = {row_7[#row_7[]-1] , row_7[#row_7[]-2]} ;
linext += {l};
l = newl;
Line(l) = {row_7[#row_7[]-2] , row_8[#row_8[]-1]} ;
linext += {l};
//Fuel 2,1
boun_2_1 = {};
l = newl;
Line(l) = {row_1[0] , row_1[1]};
boun_2_1 += {l};
l = newl;
Line(l) = { row_1[1], row_2[1]};
boun_2_1 += {l};
l = newl;
Line(l) = { row_2[1], row_2[0]};
boun_2_1 += {l};
l = newl;
Line(l) = { row_2[0], row_1[0]};
boun_2_1 += {l};
l = newl;
Line Loop(l) = {boun_2_1[]};
s = news;
surf_2_1 = s;
Plane Surface(s) = {l};
//Square fuels in the first row.
For i In {1:#row_1[]-2}
  boun~{i+2}~{1} = {};
  l = newl;
  Line(l) = {row_1[i] , row_1[i+1]};
  boun~{i+2}~{1} += {l};
  l = newl;
  Line(l) = { row_1[i+1], row_2[i+1]};
  boun~{i+2}~{1} += {l};
  l = newl;
  Line(l) = { row_2[i+1], row_2[i]};
  boun~{i+2}~{1} += {l};
  l = newl;
  Line(l) = { row_2[i], row_1[i]};
  boun~{i+2}~{1} += {l};
  l = newl;
  Line Loop(l) = {boun~{i+2}~{1}[]};
  s = news;
  surf~{i+2}~{1} = s;
  Plane Surface(s) = {l};
EndFor
//Triangular fuels
Call AsBoundTri;
//2 row of square fuels.
discount = { 2 , 2 , 2 , 3 , 2 };
For j In {2:6}
  For i In {1:#row~{j}[]-discount[j-2] }
    boun~{i+j}~{j} = {};
    l = newl;
    Line(l) = {row~{j}[i] , row~{j}[i+1]};
    boun~{i+j}~{j} += {l};
    l = newl;
    Line(l) = { row~{j}[i+1], row~{j+1}[i]};
    boun~{i+j}~{j} += {l};
    l = newl;
    Line(l) = { row~{j+1}[i], row~{j+1}[i-1]};
    boun~{i+j}~{j} += {l};
    l = newl;
    Line(l) = { row~{j+1}[i-1], row~{j}[i]};
    boun~{i+j}~{j} += {l};
    l = newl;
    Line Loop(l) = {boun~{i+j}~{j}[]};
    s = news;
    surf~{i+j}~{j} = s;
    Plane Surface(s) = {      l            };
  EndFor
EndFor
Physical Surface("External") = {};
For i In {0:#linext[]-1}
  aux[] = Extrude{0,0,hcr-heigth2}{
    Line {linext[i]};
  };
  Physical Surface("External") += {aux[1]};
  aux[] = Extrude{0,0, cri       }{
    Line { aux[0]  };
  };
  Physical Surface("External") += {aux[1]};
  aux[] = Extrude{0,0, uplay     }{
    Line { aux[0]  };
  };
  Physical Surface("External") += {aux[1]};
  aux[] = Extrude{0,0,startaz-heigth2}{
    Line {linext[i]};
  };
  Physical Surface("External") += {aux[1]};
  aux[] = Extrude{0,0,-startaz} {
    Line { aux[0] };
   };
   Physical Surface("External") += {aux[1]};
EndFor
//Vertical 0 Current surfaces. Diagonal (y=x).
Physical Surface("NoCurrent") = {};
For i In {1:7}
  aux[] = Extrude{0,0,hcr-heigth2} {
    Line { boun~{i}~{i}[2] };
   };
   Physical Surface("NoCurrent") += {aux[1]};
  aux[] = Extrude{0,0, cri } {
    Line { aux[0] };
   };
   Physical Surface("NoCurrent") += {aux[1]};
  aux[] = Extrude{0,0, uplay } {
    Line { aux[0] };
   };
   Physical Surface("NoCurrent") += {aux[1]};
//  Printf("%g",boun~{i}~{i}[2]);
EndFor
//Vertical 0 Current surfaces. y=0.
For i In {1:9}
  aux[] = Extrude{0,0,hcr-heigth2} {
  Line { boun~{i}~{1}[0] };
  };
   Physical Surface("NoCurrent") += {aux[1]};
  aux[] = Extrude{0,0, cri   } {
    Line { aux[0] };
   };
   Physical Surface("NoCurrent") += {aux[1]};
  aux[] = Extrude{0,0, uplay } {
    Line { aux[0] };
   };
   Physical Surface("NoCurrent") += {aux[1]};
EndFor
//Vertical 0 Current surfaces. Diagonal (y=x).
For i In {1:7}
  aux[] = Extrude{0,0,startaz-heigth2} {
    Line { boun~{i}~{i}[2] };
   };
   Physical Surface("NoCurrent") += {aux[1]};
  aux[] = Extrude{0,0,-startaz} {
    Line { aux[0] };
   };
   Physical Surface("NoCurrent") += {aux[1]};
EndFor
//Vertical 0 Current surfaces. y=0.
For i In {1:9}
  aux[] = Extrude{0,0,startaz-heigth2} {
  Line { boun~{i}~{1}[0] };
  };
   Physical Surface("NoCurrent") += {aux[1]};
  aux[] = Extrude{0,0,-startaz} {
    Line { aux[0] };
   };
   Physical Surface("NoCurrent") += {aux[1]};
EndFor
//Bottom half core.
Physical Volume("Reflector") = {};
volref = {};  //It could be useful to make a compound volume.
For j In {1:6}
  For i In {j:6}
    aux[] = Extrude{0,0,startaz-heigth2}{
      Surface {surf~{i}~{j}};
      };
    v=Sprintf("F_%g_%g",i,j);
    Physical Volume(Str(v)) = {aux[1]};
    vol~{i}~{j} = aux[1];
  //Reflector below the fuel.
    aux[] =Extrude{0,0,-startaz}{
      Surface{aux[0]};
      };
    Physical Volume("Reflector") += {aux[1]};
    Physical Surface("External") += {aux[0]};
    volref += {aux[1]};
  EndFor
EndFor
For j In {1:7}
  If (j<6)
    For i In {7:7}
      aux[] = Extrude{0,0,startaz-heigth2}{
        Surface {surf~{i}~{j}};
        };
      v=Sprintf("F_%g_%g",i,j);
      Physical Volume(Str(v)) = {aux[1]};
      vol~{i}~{j} = aux[1];
    //Reflector below the fuel.
      aux[] =Extrude{0,0,-startaz}{
        Surface{aux[0]};
        };
      Physical Volume("Reflector") += {aux[1]};
      Physical Surface("External") += {aux[0]};
      volref += {aux[1]};
    EndFor
  Else
    For i In {7:7}
      aux[] = Extrude{0,0,startaz-heigth2}{
        Surface {surf~{i}~{j}};
        };
      Physical Volume("Reflector") += {aux[1]};
      volref += {aux[1]};
      aux[] =Extrude{0,0,-startaz}{
        Surface{aux[0]};
        };
      Physical Volume("Reflector") += {aux[1]};
      Physical Surface("External") += {aux[0]};
      volref += {aux[1]};
    EndFor
  EndIf
EndFor
For j In {1:6}
  If (j<4)
    For i In {8:8}
      aux[] = Extrude{0,0,startaz-heigth2}{
        Surface {surf~{i}~{j}};
        };
      v=Sprintf("F_%g_%g",i,j);
      Physical Volume(Str(v)) = {aux[1]};
      vol~{i}~{j} = aux[1];
    //Reflector below the fuel.
      aux[] =Extrude{0,0,-startaz}{
        Surface{aux[0]};
        };
      Physical Volume("Reflector") += {aux[1]};
      Physical Surface("External") += {aux[0]};
      volref += {aux[1]};
    EndFor
  Else
    For i In {8:8}
      aux[] = Extrude{0,0,startaz-heigth2}{
        Surface {surf~{i}~{j}};
        };
      Physical Volume("Reflector") += {aux[1]};
      volref += {aux[1]};
      aux[] =Extrude{0,0,-startaz}{
        Surface{aux[0]};
        };
      Physical Volume("Reflector") += {aux[1]};
      Physical Surface("External") += {aux[0]};
      volref += {aux[1]};
    EndFor
  EndIf
EndFor
For j In {1:4}
  For i In {9:9}
    aux[] = Extrude{0,0,startaz-heigth2}{
      Surface {surf~{i}~{j}};
      };
    Physical Volume("Reflector") += {aux[1]};
    volref += {aux[1]};
    aux[] =Extrude{0,0,-startaz}{
      Surface{aux[0]};
      };
    Physical Volume("Reflector") += {aux[1]};
    Physical Surface("External") += {aux[0]};
    volref += {aux[1]};
  EndFor
EndFor
//Upper half core.
For j In {1:7}
  For i In {j:7}
    If ( j<6 || i<7)
      If ( ( i==3) && ( j==3) )//It happends for the partial control rod inserted fuel.
        aux[] = Extrude{0,0,hcr-heigth2}{
          Surface {surf~{i}~{j}};
          };
        v=Sprintf("F_%g_%g",i,j);
        Physical Volume(Str(v)) += {aux[1]};
        vol~{i}~{j} = aux[1];
        aux[] = Extrude{0,0,cri}{
          Surface {aux[0]};
          };
        v=Sprintf("FP3_%g_%g",i,j);
        Physical Volume(Str(v)) = {aux[1]};
        aux[] = Extrude{0,0,uplay}{
          Surface {aux[0]};
          };
        v=Sprintf("FUL_%g_%g",i,j);
        Physical Volume(Str(v)) = {aux[1]};
        Physical Surface("External") += {aux[0]};
      Else
        aux[] = Extrude{0,0,hcr-heigth2}{
          Surface {surf~{i}~{j}};
          };
        v=Sprintf("F_%g_%g",i,j);
        Physical Volume(Str(v)) += {aux[1]};
        vol~{i}~{j} = aux[1];
        aux[] = Extrude{0,0,cri}{
          Surface {aux[0]};
          };
        v=Sprintf("FP3_%g_%g",i,j);
        Physical Volume(Str(v)) = {aux[1]};
        aux[] = Extrude{0,0,uplay}{
          Surface {aux[0]};
          };
        v=Sprintf("FUL_%g_%g",i,j);
        Physical Volume(Str(v)) = {aux[1]};
        Physical Surface("External") += {aux[0]};
      EndIf
    Else
      aux[] = Extrude{0,0,hcr-heigth2}{
        Surface {surf~{i}~{j}};
        };
      Physical Volume("Reflector") += {aux[1]};
      aux[] = Extrude{0,0,cri        }{
        Surface {aux[0]      };
        };
      Physical Volume("Reflector") += {aux[1]};
      aux[] = Extrude{0,0, uplay     }{
        Surface {aux[0]      };
        };
      Physical Volume("Reflector") += {aux[1]};
      Physical Surface("External") += {aux[0]};
      volref += {aux[1]};
    EndIf
  EndFor
EndFor


For j In {1:6}
  If (j<4)
    For i In {8:8}
      aux[] = Extrude{0,0,hcr-heigth2}{
        Surface {surf~{i}~{j}};
        };
      v=Sprintf("F_%g_%g",i,j);
      Physical Volume(Str(v)) += {aux[1]};
      vol~{i}~{j} += {aux[1]};
      aux[] = Extrude{0,0,cri }{
        Surface{ aux[0]  };
        };
      v=Sprintf("F_%g_%g",i,j);
      Physical Volume(Str(v)) += {aux[1]};
      vol~{i}~{j} += {aux[1]};
    //Reflector above the fuel.
      aux[] =Extrude{0,0,uplay}{
        Surface{aux[0]};
        };
      Physical Volume("Reflector") += {aux[1]};
      Physical Surface("External") += {aux[0]};
      volref += {aux[1]};
    EndFor
  Else
    For i In {8:8}
      aux[] = Extrude{0,0,hcr-heigth2}{
        Surface {surf~{i}~{j}};
        };
      Physical Volume("Reflector") += {aux[1]};
      aux[] = Extrude{0,0,cri        }{
        Surface {aux[0]      };
        };
      Physical Volume("Reflector") += {aux[1]};
      aux[] = Extrude{0,0, uplay     }{
        Surface {aux[0]      };
        };
      Physical Volume("Reflector") += {aux[1]};
      Physical Surface("External") += {aux[0]};
      volref += {aux[1]};
    EndFor
  EndIf
EndFor
For j In {1:4}
  For i In {9:9}
    aux[] = Extrude{0,0,hcr-heigth2}{
      Surface {surf~{i}~{j}};
      };
    Physical Volume("Reflector") += {aux[1]};
    aux[] = Extrude{0,0,cri        }{
      Surface {aux[0]      };
      };
    Physical Volume("Reflector") += {aux[1]};
    aux[] = Extrude{0,0, uplay     }{
      Surface {aux[0]      };
      };
    Physical Volume("Reflector") += {aux[1]};
    Physical Surface("External") += {aux[0]};
    volref += {aux[1]};
  EndFor
EndFor
//v = newv;
//Compound Volume(v) = {volref[]};
Coherence;
Geometry.Surfaces=1;
Geometry.Volumes=1;
//Mesh.Algorithm3D = 5;      // delaunay triangulation
//Mesh.Recombine3DAll = 1;  // recombine triangles to obtain quads
//Mesh.ElementOrder = 2;
