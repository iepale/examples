\documentclass[11pt,twoside,a4paper]{article}
\usepackage[driver=dvips,margin=2cm]{geometry}
\usepackage{graphicx}
\usepackage[labelfont=bf]{caption}
\usepackage{amsmath}
%\usepackage{atbegshi}
\usepackage[dvips]{pdflscape}
\usepackage{longtable}
\usepackage[all]{xy}
\usepackage[dvips,colorlinks=true,linkcolor=blue, bookmarks=true]{hyperref}

%latex B15A1.tex ;dvips B15A1.dvi;ps2pdf B15A1.ps B15A1.pdf

\renewcommand{\theenumii}{\arabic{enumii}}
\renewcommand{\labelenumii}{\theenumi.\theenumii}
\renewcommand{\theenumiii}{\arabic{enumiii}}
\renewcommand{\labelenumiii}{\theenumi.\theenumii.\theenumiii}
\newcommand{\wasmail}{wasora@samplex.com}
\newcommand{\wasbit}{https://bitbucket.org/wasora/wasora}
\newcommand{\milbit}{https://bitbucket.org/wasora/milonga/overview}

\begin{document}
\title {\textbf{Benchmark 15-A2 calculated with milonga}}
\author{Camusso, C.P.}
\date{\today}
\maketitle
\tableofcontents 
\listoffigures
\listoftables

\vspace{0.5cm}
Copyright (c)  2016 Camusso César Pablo.
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover
Texts.  A copy of the license is included in the section entitled "GNU
Free Documentation License". \cite{Lic}

\section{Introduction}
\begin{enumerate}
\item The benchmark ANL-7416-15A2 \cite{B15} was calculated using the milonga code.
\item The function of this benchmark is to test solutions of the neutronic depletion equations.
\item It is a infinite homogeneus nuclear reactor with isotopic concentrations given. At time zero,
the neutron flux becomes nonzero.

\item The codes used were:

wasora 0.4.117  (14dccdd2711f+Δ 2016-07-18 11:38 -0300) \cite{wasora}

wasora's an advanced suite for optimization \& reactor analysis

 rev hash 14dccdd2711f7eea767f5b6a01aa509235e385e4

 last commit on 2016-07-18 11:38 -0300 (rev 272)

 compiled on 2016-07-18 21:00:58 by pablo@pablo (linux-gnu x86\_64)

 with gcc (Debian 4.9.2-10) 4.9.2 using -O2 and linked against

  GNU Scientific Library version 1.16

  SUNDIALs Library version 2.5.0

  GNU Readline version 6.3

 wasora is copyright (C) 2009-2016 jeremy theler

 licensed under GNU GPL version 3 or later.

 wasora is free software: you are free to change and redistribute it.

 There is NO WARRANTY, to the extent permitted by law.

\item You also can use milonga \cite{milonga} because it is a plugin of wasora.
\end{enumerate}
\section{Benchmark information}
\begin{enumerate}
\item Solution of isotopic depletion equations at a point with constant flux and cross sections.
\begin{equation}
\frac{d\mathbf{N}(t)}{dt} = \mathbf{A}\cdot\mathbf{N}(t)
\end{equation}
where 

\textbf{N} = vector of isotopic concentrations

\textbf{A} = net production matrix coupling isotopes
\item The general $ij^{th}$ entry in \textbf{A} (i.e., the production rate of isotope $i$ form isotope $j$) is
\begin{equation}
A_{ij} = Y_{ij}\sum_{\substack{g}}\sigma_{fj}^g\Phi^g+\lambda_{ij}+\sum_{\substack{g}}\sigma_{c_{ij}}^g\Phi^g  \label{eqn:mat}
\end{equation}
where

$g$ = energy group index

$Y_{ij}$ = fission yield of isotope $i$ from the fissioning of isotope $j$ ($Y_{ii}$ is defined as -1)

$\sigma_{fj}^g$ = microscopic fission cross section of isotope $j$ in group $g$

$\Phi^g$ = flux in group $g$

$\lambda_{ij}$ = decay constant for production of isotope $i$ from the decay of isotope $j$ ($\lambda_{ii}$ 
is the negative of the decay constant)

$\sigma_{c_{ij}}^g$ = microscopic capture cross section in group $g$ for isotope $j$ that produces $i$
($\sigma_{c_{ii}}^g$ is the negative of the capture cross section)
\item Constant two-group flux:
\begin{enumerate}
  \item Group 1 = $6.1374\cdot10^{14} \frac{n}{cm^2\;s}$
  \item Group 2 = $2.5078\cdot10^{14} \frac{n}{cm^2\;s}$
\end{enumerate}
\item Fission product yields are defined in the \autoref{tab:fisyiel}.
\item Decay constants are defined in the \autoref{tab:decay}.
\item The (n,2n) microscopic cross sections are defined in the \autoref{tab:micn2n}.
\item The initial conditions are shown in the \autoref{tab:inicial}.
\item Microscopic cross sections are defined in \autoref{tab:micro}.
\item The $\alpha$ and $\beta^+$ decay were not excluded from the depletion chain, 
see the \autoref{fig:dechain} and the \autoref{fig:pfdechain}. So \textbf{A} is not a triangular matrix \cite{B15}.
\end{enumerate}
\section{Expected results}
\begin{enumerate}
\item The benchmark asks the following results:
  \begin{enumerate}
  \item Variation of isotopic concentrations with time; 50-day concentrations.
  \item Calculational statistics.
\end{enumerate}
\end{enumerate}
\section{Solutions available}
\begin{enumerate}
\item Fourth-order Runge-Kutta of depletion: 15-A2-1 \cite{B15}
\item Matrix exponential method and finite difference solution: 15-A2-2 \cite{B15}
\end{enumerate}

\section{Solution}
\begin{enumerate}
\item The \autoref{eqn:mat} is written differently as:
\begin{equation}
A_{ij} = Y_{ij}\boldsymbol{\sigma_{fj}}\cdot\boldsymbol{\Phi}+\lambda_{ij}+\boldsymbol{\sigma_{c_{ij}}}\cdot\boldsymbol{\Phi}
\end{equation}

where 
\begin{equation}
\boldsymbol{\Phi}=
\begin{bmatrix}
6.1374\cdot10^{14}      \\
2.5078\cdot10^{14}
\end{bmatrix}  
\end{equation}

$\boldsymbol{\sigma_{fj}}$ and $\boldsymbol{\sigma_{c_{ij}}}$ for $i=13,\;j=12$ are (from \autoref{tab:micro}):
\begin{equation*}
\boldsymbol{\sigma_{f,12}} =
\begin{bmatrix}
14.403 \\
348.89
\end{bmatrix}
;\quad\boldsymbol{\sigma_{c_{13,12}}} =
\begin{bmatrix}
9.8658     \\
196.77
\end{bmatrix}
\end{equation*}
note that $\boldsymbol{\sigma_{c_{i,12}}}$ is zero when $i\neq13$. It means that \textsuperscript{239}Pu
becomes \textsuperscript{240}Pu when it absorbs a neutron. 
\item The $\beta^+$ and the $\alpha$ decay were appended to $\boldsymbol{\lambda}$. The \textsuperscript{242}Am can decay by $\beta^+$ or $\beta^-$,
so its decay constant is the sum of both ones.
\item The (n,2n) reaction was added to $\boldsymbol{\sigma_{c_{ij}}}$ in this way:

\begin{equation}
^AIsotope+n \to ^{A-1}Isotope+2n
\end{equation}
\item The results are shown in the \autoref{tab:res} with a comparison with one of the results from the solution 15-A2-1 \cite{B15}. Note that the units were translated
into $atom/cm^3$ and FP means fission products.
\item The diference in the \autoref{tab:res} is among the milonga results and the \cite{B15} one.
\item The maximum difference was in the isotope \textsuperscript{243}Cm. It is considered unimportant because the results of
the isotope \textsuperscript{242}Cm, from which \textsuperscript{243}Cm appears, and the isotope \textsuperscript{244}Cm, in which \textsuperscript{243}Cm
becomes, were similar in these results and in \cite{B15}.
\item The time evolution of each isotope's numerical density can be seen in the \autoref{fig:Ura}, the \autoref{fig:Plu}, the \autoref{fig:Np}, 
the \autoref{fig:Am}, the \autoref{fig:Cm}, the \autoref{fig:FP1}, and the \autoref{fig:FP2}.
\end{enumerate}

\section{milonga's input file}
\begin{enumerate}
\item There are two keywords which are more or less new:

rel\_error: It sets the relative numerical error in each variable. If it is too small, the calculation could not converge and finish in a messge error.

INITIAL\_CONDITIONS\_MODE FROM\_VARIABLES: The IDA library needs the derivative of the vector being solved at time zero:
$\dot{\mathbf{N}}(0)$. This keyword asks milonga
calculate it. If it were not used, the user would have to initiate $\dot{\mathbf{N}}(0)$. If not, the calculation could not converge or give a message error.
\end{enumerate}

\section{Excercise}
\begin{enumerate}
\item Print the matrices \textbf{Y}, $\boldsymbol{\sigma_{f}}$, $\boldsymbol{\lambda}$, $\boldsymbol{\sigma_{c}}$ and \textbf{A}.
\end{enumerate}

\vspace{-1.5cm}
\renewcommand{\refname}{\section{References}}
\begin{thebibliography}{99}
\bibitem {Lic} FDL licence. \url{https://www.gnu.org/licenses/fdl-1.2-standalone.html}
\bibitem {B15} ANL-7416-15A2. \url{http://www.corephysics.com/benchmarks/anl7416_benchmark15.pdf}
\bibitem {wasora} Wasora code. \url{\wasbit}
\bibitem {milonga} Milonga code. \url{\milbit}
\end{thebibliography}

\begin{table}
\centering
\caption{Fission product yield, [\%]}\label{tab:fisyiel}
\begin{tabular}{|p{5cm}|c|c|c|c|}     \hline
\textbf{Fission product}             & \multicolumn{4}{|c|}{\textbf{Fissioning isotope}}   \\ \cline{2-5}
                                     & \textbf{\textsuperscript{235}U}& \textbf{\textsuperscript{238}U}& \textbf{\textsuperscript{239}Pu}& \textbf{\textsuperscript{241}Pu} \\ \hline
\textsuperscript{135}I       & 6.17    &5.78   &    6.93   &    6.26            \\ \hline
\textsuperscript{135}Xe      &0.24     & 0.22  &  0.27     & 0.24  \\ \hline
\textsuperscript{149}Pm      &1.13     & 2.1   &  1.3      & 1.2   \\ \hline
\textsuperscript{147}Nd      &2.36     & 2.8   & 2.05      & 2.2   \\ \hline
Long-lived fission products  &90.1     &89.1   & 89.45     & 90.1  \\ \hline
\end{tabular}
\end{table}

\begin{landscape}
%\vspace{3cm}
%\caption{Depletion chains for the actinides}\label{fig:dechain}
\captionof{figure}{Depletion chains for the actinides\protect\label{fig:dechain}}
\[ \xymatrix@C=0.5cm@M=0mm{
Process: &        &       &         &         &        &               &      &    &    &                  &  \\
\ar[r]^{(n,\gamma)} &        &       &         &         &        &\ar@{-->}[dddd]&      &    &    &\ar @{--} [llll]  &  \\
\ar@{-->}[d]  & \ar@{--}[l]_\alpha &       &         &         &\ar@{-->}[ddd]    &         &      &    &\ar@{--}[llll]  &  &  \\
 & \ar@<1ex>[d]^{\beta^+} &   &      & \ar@{-->}[dd] &        &         &      & {^{242}Cm}\ar[r]\ar@{--}[llll] &{^{243}Cm}\ar[r]\ar@{--}[u] & {^{244}Cm}\ar[r]\ar @{--} [uu] & {^{245}Cm}  \\
\ar@<1ex>[u]^{\beta^-} & & & \ar@{-->}[dd] &         &    &    &{^{241}Am}\ar[r]\ar@{--}[llll]& {^{242,242m}Am}\ar[r]\ar[u]\ar[d] & {^{243}Am}\ar[r] & {^{244}Am}\ar[u] &   \\
\ar@{-->}[dd]&  &   &  &{^{238}Pu}\ar[r]\ar@{--}[llll]&{^{239}Pu}\ar[r]&{^{240}Pu}\ar[r]&{^{241}Pu}\ar[r]\ar[u]&{^{242}Pu}\ar[r]   & {^{243}Pu}\ar[u] &  & \\
&  &       & {^{237}Np}\ar[r]&{^{238}Np}\ar[r]\ar[u]&{^{239}Np}\ar[r]\ar[u]&{^{240}Np}\ar[u] &  &  &  &  &  \\
{^{234}U}\ar[r] & {^{235}U}\ar[r]& {^{236}U}\ar[r]  & {^{237}U}\ar[r]\ar[u]&{^{238}U}\ar[r]&{^{239}U}\ar[u]&  &  & &  & &  \\
} \] 

\begin{figure}[h]
\caption{Depletion chains for the fission products}\label{fig:pfdechain}
\[ \xymatrix {
^{135}Cs          &                &                  &                      &   ^{149}Sm       \\
^{135}Xe \ar[u]   &Fission \ar[l]  & ^{147}Pm \ar[r]  & ^{148,148m}Pm \ar[r] & ^{149}Pm \ar[u]  \\
^{135}I  \ar[u]   &                & ^{147}Nd \ar[u]  &                      & Fission  \ar[u]  \\
Fission  \ar[u]   &                & Fission  \ar[u]  &                      &                  \\
}\]
\end{figure}
\end{landscape}

\begin{table}[h]
\centering
\caption{Decay constants}\label{tab:decay}
\begin{tabular}{|c|c|c|}  \hline
\textbf{Isotope}         &    \textbf{Emitted particle}      &     \textbf{Decay constant, s\textsuperscript{-1}}        \\ \hline
\textsuperscript{135}I       & $\beta^-$       &  $2.874\cdot10^{-5}$              \\ \hline
\textsuperscript{135}Xe      & $\beta^-$       &  $2.093\cdot10^{-5}$              \\ \hline
\textsuperscript{147}Nd      & $\beta^-$       &  $7.228\cdot10^{-7}$              \\ \hline
\textsuperscript{147}Pm      & $\beta^-$       &  $8.289\cdot10^{-9}$              \\ \hline
\textsuperscript{148}Pm      & $\beta^-$       &  $1.488\cdot10^{-6}$              \\ \hline
\textsuperscript{148m}Pm     & $\beta^-$       &  $1.976\cdot10^{-7}$              \\ \hline
\textsuperscript{149}Pm      & $\beta^-$       &  $3.626\cdot10^{-6}$              \\ \hline
\textsuperscript{237}U       & $\beta^-$       &  $1.19\cdot10^{-6}$               \\ \hline
\textsuperscript{239}U       & $\beta^-$       &  $4.915\cdot10^{-4}$              \\ \hline
\textsuperscript{238}Np      & $\beta^-$       &  $3.82\cdot10^{-6}$               \\ \hline
\textsuperscript{239}Np      & $\beta^-$       &  $3.41\cdot10^{-6}$               \\ \hline
\textsuperscript{240}Np      & $\beta^-$       &  $1.583\cdot10^{-3}$              \\ \hline
\textsuperscript{238}Pu      & $\alpha $       &  $2.55\cdot10^{-10}$              \\ \hline
\textsuperscript{241}Pu      & $\beta^-$       &  $1.68\cdot10^{-9}$               \\ \hline
\textsuperscript{243}Pu      & $\beta^-$       &  $3.886\cdot10^{-5}$              \\ \hline
\textsuperscript{241}Am      & $\alpha $       &  $5.09\cdot10^{-11}$               \\ \hline
\textsuperscript{242}Am      & $\beta^-$       &  $9.93\cdot10^{-6}$               \\ \hline
\textsuperscript{242}Am      & $\beta^+$       &  $2.03\cdot10^{-6}$               \\ \hline
\textsuperscript{244}Am      & $\beta^-$       &  $4.44\cdot10^{-4}$               \\ \hline
\textsuperscript{242}Cm      & $\alpha $       &  $4.91\cdot10^{-8}$               \\ \hline
\textsuperscript{243}Cm      & $\alpha $       &  $6.86\cdot10^{-10}$               \\ \hline
\textsuperscript{244}Cm      & $\alpha $       &  $1.25\cdot10^{-9}$               \\ \hline
\end{tabular}
\end{table}

\begin{table}[h]
\centering
\caption{(n,2n) Microscopic Cross Sections (Group 1 only)}\label{tab:micn2n}
\begin{tabular}{|c|c|}     \hline
\textbf{Isotope}      &     $\sigma [10^{-24} cm^2]$   \\ \hline
\textsuperscript{235}U      &0.002603                  \\ \hline
\textsuperscript{238}U      &0.0043972                 \\ \hline
\textsuperscript{237}Np     &0.00020144                \\ \hline
\end{tabular}
\end{table}


\begin{center}
\captionof{table}{Initial conditions}\label{tab:inicial}
\begin{tabular}{|c|c|}        \hline
\textbf{Isotope}        &  \textbf{Concentration} [atom/cm\textsuperscript{3}]      \\  \hline
\textsuperscript{235}U  & 0.74003 10\textsuperscript{20}    \\  \hline
\textsuperscript{238}U  & 0.6936 10\textsuperscript{22}     \\  \hline
\end{tabular}
\end{center}

\begin{table}[!ht]
\centering
%\captionof{table}{Microscopic cross sections, [barns]}\label{tab:micro}
\caption{Microscopic cross sections, [barns]}\label{tab:micro}
\begin{tabular}{|c|c|c|c|c|c|c|c|}  \hline
\textbf{Isotope} & \textbf{Isotope} &$\boldsymbol{\sigma_{c_1}}$ & $\boldsymbol{\sigma_{c_2}}$ & $\boldsymbol{\sigma_{f_1}}$ & $\boldsymbol{\sigma_{f_2}}$ & $\boldsymbol{\sigma^m_{(n,\gamma)_1}}$ & $\boldsymbol{\sigma^m_{(n,\gamma)_2}}$           \\ 
                 & \textbf{index  } &     &   &    &  &    &    \\ \hline
\textsuperscript{234}U  &  1 & 33.575  &  26.368  &  0.42744  &  0      &  0      &  0            \\ \hline
\textsuperscript{235}U  &  2 & 5.9872  &  26.42   & 12.37     & 148.18  &      0  &  0            \\ \hline
\textsuperscript{236}U  &  3 & 16.859  &  1.4399  & 0.16664   &  0      &      0  &  0            \\ \hline
\textsuperscript{237}U  &  4 & 16.991  &  132.12  & 0.17139   & 0.55512 &      0  &  0            \\ \hline
\textsuperscript{238}U  &  5 & 0.53258 & 0.73141  & 0.081338  &  0      &      0  &  0            \\ \hline
\textsuperscript{239}U  &  6 & 0.40015 & 6.1613   & 0.27283   & 4.2009  &      0  &  0            \\ \hline
\textsuperscript{237}Np &  7 & 24.072  & 71.864   & 0.41867   & 0.009425&      0  &  0            \\ \hline
\textsuperscript{238}Np &  8 & 5.2648  & 55.512   & 47.412    & 555.12  &      0  &  0            \\ \hline
\textsuperscript{239}Np &  9 & 26.341  & 16.654   & 0         &    0    &      0  &  0            \\ \hline
\textsuperscript{240}Np & 10 &  0      &    0     & 0         &    0    &      0  &  0            \\ \hline
\textsuperscript{238}Pu & 11 & 7.3125  & 119.91   & 1.5815    & 3.5496  &      0  &  0            \\ \hline
\textsuperscript{239}Pu & 12 & 9.8658  & 196.77   & 14.403    & 348.89  &      0  &  0            \\ \hline
\textsuperscript{240}Pu & 13 & 366.09  & 96.479   & 0.54033   & 0.016744&      0  &  0            \\ \hline
\textsuperscript{241}Pu & 14 & 8.0305  & 152.24   & 29.986    & 352.73  &      0  &  0            \\ \hline
\textsuperscript{242}Pu & 15 & 51.82   & 5.1903   & 0.4346    & 0       &      0  &  0            \\ \hline
\textsuperscript{243}Pu & 16 & 11.03   & 21.649   & 28.382    & 49.96   &      0  &  0            \\ \hline
\textsuperscript{241}Am & 17 & 50.633  & 392.68   & 1.113     & 2.3817  & 6.7486  & 39.543        \\ \hline
\textsuperscript{242}Am & 18 & 2.3381  &     0    & 31.137    & 693.9   &      0  &  0            \\ \hline
\textsuperscript{242m}Am& 19 & 20.016  & 444.09   & 108.79    & 1776.4  &      0  &  0            \\ \hline
\textsuperscript{243}Am & 20 & 91.056  & 24.08    & 0.30784   &  0      &      0  &  0            \\ \hline
\textsuperscript{244}Am & 21 &  0      &    0     & 26.192    & 403.29  &      0  &  0            \\ \hline
\textsuperscript{242}Cm & 22 & 3.1202  & 1.7185   &   0       & 0.83267 &      0  &  0            \\ \hline
\textsuperscript{243}Cm & 23 & 9.9059  & 69.389   & 92.299    & 194.29  &      0  &  0            \\ \hline
\textsuperscript{244}Cm & 24 & 32.129  & 3.6915   & 1.5663    & 0.33307 &      0  &  0            \\ \hline
\textsuperscript{245}Cm & 25 & 4.8993  & 82.972   & 37.165    & 537.15  &      0  &  0            \\ \hline
\textsuperscript{135}I  & 26 &   0     &   0      &   0       &   0     &      0  &  0            \\ \hline
\textsuperscript{135}Xe & 27 & 243.47  & 1064780  &   0       &   0     &      0  &  0            \\ \hline
\textsuperscript{147}Nd & 28 &   0     &    0     &   0       &   0     &      0  &  0            \\ \hline
\textsuperscript{147}Pm & 29 & 248.78  & 65.814   &   0       &   0     & 114.44  & 31.087        \\ \hline
\textsuperscript{148}Pm & 30 & 3368.4  & 420.09   &   0       &   0     &      0  &  0            \\ \hline
\textsuperscript{148m}Pm& 31 & 2921    & 7561.6   &   0       &   0     &      0  &  0            \\ \hline
\textsuperscript{149}Pm & 32 &   0     &   0      &   0       &   0     &      0  &  0            \\ \hline
\textsuperscript{149}Sm & 33 & 105.85  & 23387.4  &   0       &   0     &      0  &  0            \\ \hline
Fission products        & 34 & 10.376  & 19.429   &   0       &   0     &      0  &  0            \\ \hline
\end{tabular}

\vspace{1cm}
\raggedright

$\sigma_{c_1}$ = capture in group 1 (all captures except fission and (n,2n); includes (n,$\gamma$) to excited
state, if any).

$\sigma_{c_2}$ = capture in group 2.


$\sigma_{f_1}$ = fission in group 1.

$\sigma_{f_2}$ = fission in group 2.

$\sigma^m_{(n,\gamma)_1}$ = (n,$\gamma$) to first excited state, group 1.

$\sigma^m_{(n,\gamma)_2}$ = (n,$\gamma$) to first excited state, group 2.

\end{table}
\begin{table}[p]
\centering
\caption{Benchmark results}\label{tab:res}
\begin{tabular}{|c|c|c|c|c|c|}        \hline
\textbf{Isotope}     &  \multicolumn{2}{|c|}{\textbf{milonga maximum }$\boldsymbol{\Delta t}$} & \textbf{15-A2-1} \cite{B15} &\multicolumn{2}{|c|}{\textbf{Difference [\%]} }   \\  \cline{2-6}
      &  1 hour    &    1 day       & 1 min         & 1 hour  & 1 day     \\ \hline
\textsuperscript{234}U  & 4.29017 10\textsuperscript{14} &   10\textsuperscript{14}  &4.28817 10\textsuperscript{14} & 4.66 10\textsuperscript{-2}   &  1.43   \\ \hline
\textsuperscript{235}U  & 5.83315 10\textsuperscript{19} &   10\textsuperscript{19}  &5.83393 10\textsuperscript{19} & -1.34 10\textsuperscript{-2}& -4.1 10\textsuperscript{-1}\\ \hline
\textsuperscript{236}U  & 2.86193 10\textsuperscript{18} &   10\textsuperscript{18}  &2.86054 10\textsuperscript{18} & 4.86 10\textsuperscript{-2} & 1.48                        \\ \hline
\textsuperscript{237}U  & 3.5687  10\textsuperscript{16} &   10\textsuperscript{16}  &3.56768 10\textsuperscript{16} & 2.86 10\textsuperscript{-2} & 1.09                        \\ \hline
\textsuperscript{238}U  & 6.91915 10\textsuperscript{21} &   10\textsuperscript{21}  &6.91916 10\textsuperscript{21} & -1.45 10\textsuperscript{-4} & -4.19 10\textsuperscript{-3}\\ \hline
\textsuperscript{239}U  & 7.18361 10\textsuperscript{15} &   10\textsuperscript{15}  &7.18357 10\textsuperscript{15} & 5.57 10\textsuperscript{-4} & -3.48 10\textsuperscript{-3}\\ \hline
\textsuperscript{237}Np & 1.04823 10\textsuperscript{17} &   10\textsuperscript{17}  &1.04736 10\textsuperscript{17} & 8.31 10\textsuperscript{-2} & 2.78                        \\ \hline
\textsuperscript{238}Np & 7.8119  10\textsuperscript{14} &   10\textsuperscript{14}  &7.80485 10\textsuperscript{14} & 9.03 10\textsuperscript{-2} & 2.96                        \\ \hline
\textsuperscript{239}Np & 1.02944 10\textsuperscript{18} &   10\textsuperscript{18}  &1.02944 10\textsuperscript{18} & 0                           & -3.89 10\textsuperscript{-3}\\ \hline
\textsuperscript{240}Np & 1.32293 10\textsuperscript{13} &   10\textsuperscript{13}  &1.32292 10\textsuperscript{13} & -7.56 10\textsuperscript{-4}& -3.78 10\textsuperscript{-3}\\ \hline
\textsuperscript{238}Pu & 4.42512 10\textsuperscript{15} &   10\textsuperscript{15}  &4.41854 10\textsuperscript{15} & 1.49 10\textsuperscript{-1} & 4.82                        \\ \hline
\textsuperscript{239}Pu & 1.05792 10\textsuperscript{19} &   10\textsuperscript{19}  &1.05748 10\textsuperscript{19} & 4.16 10\textsuperscript{-2} & 1.33                        \\ \hline
\textsuperscript{240}Pu & 9.96774 10\textsuperscript{17} &   10\textsuperscript{17}  &9.95892 10\textsuperscript{17} & 8.86 10\textsuperscript{-2} & 2.69                        \\ \hline
\textsuperscript{241}Pu & 3.3467  10\textsuperscript{17} &   10\textsuperscript{17}  &3.34195 10\textsuperscript{17} & 1.42 10\textsuperscript{-1} & 4.45                        \\ \hline
\textsuperscript{242}Pu & 1.64071 10\textsuperscript{16} &   10\textsuperscript{16}  &1.63736 10\textsuperscript{16} & 2.05 10\textsuperscript{-1} & 6.45                        \\ \hline
\textsuperscript{243}Pu & 1.36631 10\textsuperscript{13} &   10\textsuperscript{13}  &1.36348 10\textsuperscript{13} & 2.08 10\textsuperscript{-1} & 6.5                         \\ \hline
\textsuperscript{241}Am & 5.8755  10\textsuperscript{14} &   10\textsuperscript{14}  &5.8639  10\textsuperscript{14} & 1.98 10\textsuperscript{-1} & 6.29                        \\ \hline
\textsuperscript{242}Am & 5.2204  10\textsuperscript{12} &   10\textsuperscript{12}  &5.20984 10\textsuperscript{12} & 2.03 10\textsuperscript{-1} & 6.43                        \\ \hline
\textsuperscript{242m}Am& 5.06022 10\textsuperscript{12} &   10\textsuperscript{12}  &5.04819 10\textsuperscript{12} & 2.38 10\textsuperscript{-1} & 7.63                        \\ \hline
\textsuperscript{243}Am & 4.58325 10\textsuperscript{14} &   10\textsuperscript{14}  &4.57037 10\textsuperscript{14} & 2.82 10\textsuperscript{-1} & 8.46                        \\ \hline
\textsuperscript{244}Am & 6.37468 10\textsuperscript{10} &   10\textsuperscript{10}  &6.37996 10\textsuperscript{10} & -8.28 10\textsuperscript{-2}& 8.08                        \\ \hline
\textsuperscript{242}Cm & 4.51091 10\textsuperscript{13} &   10\textsuperscript{13}  &4.49667 10\textsuperscript{13} & 3.17 10\textsuperscript{-1} & 8.5                         \\ \hline
\textsuperscript{243}Cm & 7.22665 10\textsuperscript{10} &   10\textsuperscript{10}  &9.50949 10\textsuperscript{10} & -2.40 10\textsuperscript{+1}& -1.64 10\textsuperscript{+1}\\ \hline
\textsuperscript{244}Cm & 2.06679 10\textsuperscript{13} &   10\textsuperscript{13}  &2.06737 10\textsuperscript{13} & -2.81 10\textsuperscript{-2}& 1.01 10\textsuperscript{+1} \\ \hline
\textsuperscript{245}Cm & 2.43378 10\textsuperscript{11} &   10\textsuperscript{11}  &2.43318 10\textsuperscript{11} & 2.47 10\textsuperscript{-2} & 1.19 10\textsuperscript{+1} \\ \hline
\textsuperscript{135}I  & 8.82797 10\textsuperscript{15} &   10\textsuperscript{15}  &8.82738 10\textsuperscript{15} & 6.68 10\textsuperscript{-3} & 1.53 10\textsuperscript{-1} \\ \hline
\textsuperscript{135}Xe & 9.14804 10\textsuperscript{14} &   10\textsuperscript{14}  &9.14753 10\textsuperscript{14} & 5.58 10\textsuperscript{-3} & 1.52 10\textsuperscript{-1} \\ \hline
\textsuperscript{147}Nd & 1.21169 10\textsuperscript{17} &   10\textsuperscript{17}  &1.21154 10\textsuperscript{17} & 1.24 10\textsuperscript{-2} & 3.26 10\textsuperscript{-1} \\ \hline
\textsuperscript{147}Pm & 2.01936 10\textsuperscript{17} &   10\textsuperscript{17}  &2.0181  10\textsuperscript{17} & 6.24 10\textsuperscript{-2} & 1.90                        \\ \hline
\textsuperscript{148}Pm & 4.57349 10\textsuperscript{15} &   10\textsuperscript{15}  &4.57029 10\textsuperscript{15} & 7    10\textsuperscript{-2} & 2.11                        \\ \hline
\textsuperscript{148m}Pm& 3.8698  10\textsuperscript{15} &   10\textsuperscript{15}  &3.86727 10\textsuperscript{15} & 6.54 10\textsuperscript{-2} & 2.09                        \\ \hline
\textsuperscript{149}Pm & 1.99734 10\textsuperscript{16} &   10\textsuperscript{16}  &1.99679 10\textsuperscript{16} & 2.75 10\textsuperscript{-2} & 8.26 10\textsuperscript{-1} \\ \hline
\textsuperscript{149}Sm & 1.19809 10\textsuperscript{16} &   10\textsuperscript{16}  &1.19776 10\textsuperscript{16} & 2.76 10\textsuperscript{-2} & 8.54 10\textsuperscript{-1} \\ \hline
FP                      & 1.46224 10\textsuperscript{19} &   10\textsuperscript{19}  &1.45225 10\textsuperscript{19} & 6.88 10\textsuperscript{-1} & 2.41                        \\ \hline
Final time [days] & 50.027 &50.862  &50  &  5.4 10\textsuperscript{-2} & 1.72      \\ \hline

\end{tabular}
\end{table}

\clearpage
\noindent
\begin{minipage}{16cm}
\captionof{figure}{U numerical densities}\label{fig:Ura}
\includegraphics[width=16cm]{U}
\end{minipage}

\vspace{1cm}

\noindent
\begin{minipage}{16cm}
\captionof{figure}{Pu numerical densities}\label{fig:Plu}
\includegraphics[width=16cm]{Pu}
\end{minipage}

\newpage
\noindent
\begin{minipage}{16cm}
\captionof{figure}{Np numerical densities}\label{fig:Np}
\includegraphics[width=16cm]{Np}
\end{minipage}

\vspace{1cm}

\noindent
\begin{minipage}{16cm}
\captionof{figure}{Am numerical densities}\label{fig:Am}
\includegraphics[width=16cm]{Am}
\end{minipage}

\newpage
\noindent
\begin{minipage}{16cm}
\captionof{figure}{Cm numerical densities}\label{fig:Cm}
\includegraphics[width=16cm]{Cm}
\end{minipage}

\vspace{1cm}

\noindent
\begin{minipage}{16cm}
\captionof{figure}{\textsuperscript{135}I, \textsuperscript{135}Xe and FP numerical densities}\label{fig:FP1}
\includegraphics[width=16cm]{FP1}
\end{minipage}

\newpage
\noindent
\begin{minipage}{16cm}
\captionof{figure}{\textsuperscript{147}Nd, \textsuperscript{147}Pm, \textsuperscript{148, 148m}Pm, \textsuperscript{149}Pm and \textsuperscript{149}Sm numerical densities}\label{fig:FP2}
\includegraphics[width=16cm]{FP2}
\end{minipage}

\end{document}

