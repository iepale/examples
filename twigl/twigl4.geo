// Macro to set a equal spaced point on a line in the xy plane at z=0.
//The input variables are:
//dirx: x direction and x distance between neighbour points.
//diry: y direction and y distance between neoghbour points.
//x0: initial point in x.
//y0: initial point in y.
//npoints: the number of points.
//The output variables are:
//listpoints[]: all the points number in the same order as the were created.
Macro PointLine
nlistp = #listpoints[];
For i In {0:npoints-1}
//  Printf("%g %g",nlistp,i);
  listpoints[] += {newp};
  Point(listpoints[i+nlistp]) = { x0 + i * dirx , y0 + i * diry , 0.0, lc};
//  Printf("%g",i);
EndFor
Return
lc =    2.0;
lado = 160.0;
Point(1) = {  0    , 0     , 0 , lc };
Point(2) = {  lado/2 , 0     , 0 , lc };
Point(3) = {  lado/2 , lado/2  , 0 , lc };
Point(4) = {  0    , lado/2  , 0 , lc };
Point(5) = {  24   , 24    , 0 , lc };
Point(6) = {  56   , 24    , 0 , lc };
Point(7) = {  80   , 24    , 0 , lc };
Point(8) = {  0    , 56    , 0 , lc };
Point(9) =  {  24   , 56    , 0 , lc };
Point(10) = {  56   , 56    , 0 , lc };
Point(11) = {  80   , 56    , 0 , lc };
Point(12) = {  0    , 24    , 0 , lc };
Point(13) = {  24   , 80    , 0 , lc };
Point(14) = {  56   , 80    , 0 , lc };
Point(15) = {  24   ,  0    , 0 , lc };
Point(16) = {  56   ,  0    , 0 , lc };
//Fuels' boundary.
Line(1) = { 1 , 12 };
Line(2) = { 12 , 8 };
Line(3) = { 8 , 4 };
Line(4) = { 4 , 13 };
Line(5) = { 13 , 14 };
Line(6) = { 14 , 3 };
Line(7) = { 3 , 11 };
Line(8) = { 11 , 7 };
Line(9) = { 7 , 2 };
Line(10) = { 2 , 16 };
Line(11) = { 16 , 15 };
Line(12) = { 15 , 1 };
l1 = newll;
//  Line Loop(l1) = { 1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 , 10 , 11 , 12 };  
Physical Line("Left") = { 1 , 2 , 3 };  
Physical Line("Top") = { 4 , 5 , 6 };  
Physical Line("Right") = { 7 , 8 , 9 };  
Physical Line("Bottom") = { 10 , 11 , 12 };  
Line(13) = { 12 , 5 };
Line(14) = { 5 , 15 };
l2 = newll;
Line Loop(l2) = { 1 , 13 , 14 , 12 };  
s1 = news;
Plane Surface(s1) = { l2 };
Line(15) = { 8 ,  9 };
Line(16) = { 9 , 10 };
Line(17) = { 10 ,  6 };
Line(18) = { 6  , 16 };
l1 = newll;
Line Loop(l1) = { 3 , 4 , 5 , 6 , 7 , 8 , 9 , 10 , -18 , -17 , -16 , -15 };  
s2 = news;
Plane Surface(s2) = { l1 };
Physical Surface("FUEL3") = { s1 , s2 };
Line(19) = { 5  ,  9 };
Line(20) = { 5  ,  6 };
l1 = newll;
Line Loop(l1) = {  2  , 15 ,-19 ,-13 };  
s1 = news;
Plane Surface(s1) = { l1 };
s2[] = {s1};
l1 = newll;
Line Loop(l1) = { -14 , 20 , 18 , 11 };  
s1 = news;
Plane Surface(s1) = { l1 };
s2[] += {s1};
Physical Surface("FUEL2") = { s2[] };
l1 = newll;
Line Loop(l1) = { 19  , 16 , 17 , -20 };  
s1 = news;
Plane Surface(s1) = { l1 };
s2[] = {s1};
Physical Surface("FUEL1") = { s2[] };
//   l1 = newll;
//   Line Loop(l1) = { -28 , 10 , 11 ,-26 };  
//   s1 = news;
//   Plane Surface(s1) = { l1 };
//   s2[] += {s1};
//   
//   
//   l1 = newll;
//   Line Loop(l1) = { -18 , 19 , 20 , 15 };  
//   s1 = news;
//   Plane Surface(s1) = { l1 };
//   s2[] = {s1};
//   l1 = newll;
//   Line Loop(l1) = {  6  , 22 , 23 ,-17 };  
//   s1 = news;
//   Plane Surface(s1) = { l1 };
//   s2[] += {s1};
//   l1 = newll;
//   Line Loop(l1) = { -27 ,  9 , 28 ,-24 };  
//   s1 = news;
//   Plane Surface(s1) = { l1 };
//   s2[] += {s1};
//   l1 = newll;
//   Line Loop(l1) = { -25 , 26 , 12 ,-21 };  
//   s1 = news;
//   Plane Surface(s1) = { l1 };
//   s2[] += {s1};

// Field[1] = Attractor;
// Field[1].EdgesList = { 13 , 14 ,15 , 16 ,17 ,18 };
// Field[1].NodesList = { 12 ,5, 15 ,8 ,9 , 10, 6 , 16 };
// Field[1].NNodesByEdge = 20  ;
// Field[2] = Threshold;
// Field[2].IField = 1;
// Field[2].LcMin = 1.0;
// Field[2].LcMax = 5.0;
// Field[2].DistMin = 2.0 ;
// Field[2].DistMax = 2.0;
// Background Field = 2;
// Field[2] = MathEval;
// Field[2].F = "10.0";
// Field[10] = Min;
// Field[10].FieldsList = { 1 , 2};
// Background Field = 10;
Geometry.Surfaces=1;
//Mesh.Algorithm = 8;
//Mesh.Algorithm = 5;
Mesh.ElementOrder = 2;
//TransfQuadTri "*";
//Mesh.RecombineAll = 1;
//Mesh.SubdivisionAlgorithm = 2;
//Transfinite Surface "*";  
