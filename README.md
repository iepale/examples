### What is this repository for? ###

* Quick summary
    It contains the benchmarks solved with wasora and milonga:

    The reports, input files and scripts used.

    They were intended to be more or less tutorials.

### Install ###

1. Go to the directory which will contain the repository.

2. Clone it:

    `git clone https://iepale@bitbucket.org/iepale/examples.git examples`

## Examples with current oficial milonga version

### Benchmark ANL11A1 ###

#### Getting the results ####

1. Go to the anl11a2 directory:

    `cd anl11a2`

2. There are three directories:

    2.1 lc6

    2.2 lc8

    2.3 lc10

    They show the characteristic lenght used in the geo file.

    Go to one of these directories.

3. Create the mesh with

    `gmsh -3 3dpwr.geo`

4. Run milonga

    `milonga 3dpwruns.mil`

5. Run gnuplot

    `gnuplot figus.gnp`

6. Repeat the steps 3-5 for all the ls* directories. You will find a lot of files with the figures and
the answers to the questions asked by the benchmark.

#### Comparing results ####

* The benchmark results are in [ANL-7416-11A2](http://www.corephysics.com/benchmarks/anl7416_benchmark11.pdf)

    Also the first version of latex B11A2.tex and its [pdf](https://bitbucket.org/iepale/examples/downloads/B11A2.pdf) have it.

### Benchmark ANL15A1 ###

#### Getting the results ####

1. Go to the anl15a1 directory:

    `cd anl15a1`

2. Run wasora

    `wasora B15A1.was`

    You will have the results is the standard output.

#### Comparing results ####

* The benchmark results are in [ANL-7416-15A1](http://www.corephysics.com/benchmarks/anl7416_benchmark15.pdf)

    Also the first version of latex B15A1.tex and its [pdf](https://bitbucket.org/iepale/examples/downloads/B15A1.pdf) have it.

### Benchmark ANL15A2 ###

1. Go to the anl15a2 directory:

    `cd anl15a2`

2. Run wasora

    `wasora B15A2.was`

    You will have the results is the standard output.

#### Comparing results ####

* The benchmark results are in [ANL-7416-15A2](http://www.corephysics.com/benchmarks/anl7416_benchmark15.pdf)

    Also the first version of latex B15A2.tex and its [pdf](https://bitbucket.org/iepale/examples/downloads/B15A2.pdf) have it (I forgot to fill a column of the Table 6).

## Examples with milonga transients

You need to intall the fork/branch milo-tra to do these examples.

milo-tra calculates diffusion transients with milonga.

New keywords had to be added. They are:

**TS\_TYPE**: it selects the numerical method used to solve the equations. For example beuler is the backwards Euler method and cn is the Crank Nicolson method.
Petsc supports more methods but not all work.

**PRECURSOR\_GROUPS**: it selects the number of neutron delayed precursor groups.

**NOTE**: The transients only can be ran in finite elements scheme because it is being developed. Therefore it can change in the future.

### Twigl benchmark

It is a two dimenssional benchmark composed by two cases.

The first case is a step change is the absorption cross section; whereas the second case is a ramp perturbation.

#### Getting the results ####

1. Go to the twigl directory:

    `cd twigl`

    You will find the files twigl4.mil and twigl4.geo.

2. Run gmsh

    `gmsh -2 twigl4.geo`

3. Run milonga

    `milonga twigl4.mil`

    It runs the step perturbation case. Modify the file twigl4.mil to calculate the ramp perturbation case.

#### Comparing results ####

* This benchmark is very known, so look for it in iternet.

### Infinite slab reactor model

There are four cases.

#### Getting the results ####

1. Go to the id6 directory:

    `cd id6`

    You will find B6.geo, B6A1.mil, B6A2.mil, B6A3.mil and B6A4.mil files.

    The file B6.geo is the gmsh file.

    The files B6A*.mil are the four cases: one is subcritical and the remaining three are supercritical.

2. Run gmsh

    `gmsh -1 B6.geo`

3. Run milonga

    `milonga B6A1.mil`

    Repeat it for all the cases you want.
### Contribution guidelines ###

* Do the examples
* Compare with the results

### Who do I talk to? ###

* Write to wasora mailing list (wasora@seamplex.com)
