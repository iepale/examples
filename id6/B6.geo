a = 160.0;
l = 40.0;
lc = 2  ;

Point(1) = {0    ,   0, 0, lc};
Point(2) = {l     ,  0, 0, lc};
Point(3) = {l+a   ,  0, 0, lc};
Point(4) = {l+a+l ,  0, 0, lc};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};

Physical Point("left") = {1};
Physical Point("right") = {4};
Physical Line("zone1") = {1};
Physical Line("centre") = {2};
Physical Line("zone3") = {3};
Mesh.ElementOrder = 2;
